package main

import (
	"context"
	"log"
	"net"

	"github.com/xlab/closer"
	lm "gitlab.com/martyn.andrw/microlink/internal/link_modifier"
	nsql "gitlab.com/martyn.andrw/microlink/internal/nosql"
	pkg "gitlab.com/martyn.andrw/microlink/pkg"
	"google.golang.org/grpc"
)

const (
	port string = ":8082"
)

type server struct {
	pkg.UnimplementedLinkServiceServer
}

func (s *server) ShortenLink(ctx context.Context, request *pkg.ModifyLinkRequest) (*pkg.ModifyLinkResponse, error) {
	log.Printf("Received: %+v", request)

	if request.Wishes != nil {
		modified := lm.ConvertURL(*request.Wishes)
		if booked, _ := nsql.IsBooked(*modified); booked {
			err := "That short/long form has been taken. Choose another one"
			log.Printf("Error while checking: %v", err)
			return &pkg.ModifyLinkResponse{ModifiedLink: "", Error: &err}, nil
		}

		nsql.Add(*modified, request.ActualLink, "short")
		log.Printf("Sended: %v", modified)
		return &pkg.ModifyLinkResponse{ModifiedLink: *modified}, nil
	}

	modified, err := lm.ShortenLink(request.ActualLink)
	if err != nil {
		errRes := err.Error()
		log.Printf("Error while shorting: %v", errRes)
		return &pkg.ModifyLinkResponse{ModifiedLink: "", Error: &errRes}, nil
	}
	modified_link := lm.ConvertURL(*modified)

	nsql.Add(*modified_link, request.ActualLink, "short")
	log.Printf("Sended: %v", modified_link)
	return &pkg.ModifyLinkResponse{ModifiedLink: *modified_link}, nil
}

func (s *server) UnShortenLink(ctx context.Context, request *pkg.ModifyLinkRequest) (*pkg.ModifyLinkResponse, error) {
	log.Printf("Received: %+v", request)

	exist, err := nsql.IsBooked(request.ActualLink)
	if err != nil {
		errRes := err.Error()
		log.Printf("Error while checking: %v", errRes)
		return &pkg.ModifyLinkResponse{ModifiedLink: "", Error: &errRes}, nil
	}
	if !exist {
		errRes := "Actual link not founded. Try another one"
		log.Printf("not founded: %+v", request)
		return &pkg.ModifyLinkResponse{ModifiedLink: "", Error: &errRes}, nil
	}
	str, err := nsql.GetActual(request.ActualLink, "short")
	if err != nil {
		errRes := err.Error()
		log.Printf("Error while checking: %v", errRes)
		return &pkg.ModifyLinkResponse{ModifiedLink: "", Error: &errRes}, nil
	}
	if str == nil {
		errRes := "Actual link not founded. Try another one"
		log.Printf("Actual link not founded.")
		return &pkg.ModifyLinkResponse{ModifiedLink: "", Error: &errRes}, nil
	}

	log.Printf("Sended: %v", *str)
	return &pkg.ModifyLinkResponse{ModifiedLink: *str}, nil
}

func (s *server) LengthenLink(ctx context.Context, request *pkg.ModifyLinkRequest) (*pkg.ModifyLinkResponse, error) {
	log.Printf("Received: %+v", request)

	if request.Wishes != nil {
		modified := lm.ConvertURL(*request.Wishes)
		if booked, _ := nsql.IsBooked(*modified); booked {
			err := "That short/long form has been taken. Choose another one"
			log.Printf("Error while checking: %v", err)
			return &pkg.ModifyLinkResponse{ModifiedLink: "", Error: &err}, nil
		}

		nsql.Add(*modified, request.ActualLink, "long")
		log.Printf("Sended: %v", modified)
		return &pkg.ModifyLinkResponse{ModifiedLink: *modified}, nil
	}

	modified, err := lm.LengthenLink(request.ActualLink)
	if err != nil {
		errRes := err.Error()
		log.Printf("Error while lengthing: %v", errRes)
		return &pkg.ModifyLinkResponse{ModifiedLink: "", Error: &errRes}, nil
	}
	modified_link := lm.ConvertURL(*modified)

	nsql.Add(*modified_link, request.ActualLink, "long")
	log.Printf("Sended: %v", modified_link)
	return &pkg.ModifyLinkResponse{ModifiedLink: *modified_link}, nil
}

func (s *server) UnLengthenLink(ctx context.Context, request *pkg.ModifyLinkRequest) (*pkg.ModifyLinkResponse, error) {
	log.Printf("Received: %+v", request)

	exist, err := nsql.IsBooked(request.ActualLink)
	if err != nil {
		errRes := err.Error()
		log.Printf("Error while checking: %v", errRes)
		return &pkg.ModifyLinkResponse{ModifiedLink: "", Error: &errRes}, nil
	}
	if !exist {
		errRes := "Actual link not founded. Try another one"
		log.Printf("not founded: %+v", request)
		return &pkg.ModifyLinkResponse{ModifiedLink: "", Error: &errRes}, nil
	}
	str, err := nsql.GetActual(request.ActualLink, "long")
	if err != nil {
		errRes := err.Error()
		log.Printf("Error while checking: %v", errRes)
		return &pkg.ModifyLinkResponse{ModifiedLink: "", Error: &errRes}, nil
	}
	if str == nil {
		errRes := "Actual link not founded. Try another one"
		log.Printf("Actual link not founded.")
		return &pkg.ModifyLinkResponse{ModifiedLink: "", Error: &errRes}, nil
	}

	log.Printf("Sended: %v", *str)
	return &pkg.ModifyLinkResponse{ModifiedLink: *str}, nil
}

func saveDataNsql() {
	log.Println("Saving db...")
	if err := nsql.SaveData(); err != nil {
		log.Fatal(err)
	} else {
		log.Println("db.json has been saved")
	}
}

func main() {
	closer.Bind(saveDataNsql)
	err := nsql.LoadFromDB()
	if err != nil {
		log.Fatal(err)
	}

	lis, err := net.Listen("tcp", port)
	if err != nil {
		log.Fatalf("failed to listen: %v", err)
	}
	s := grpc.NewServer()
	pkg.RegisterLinkServiceServer(s, &server{})
	if err := s.Serve(lis); err != nil {
		log.Fatalf("failed to serve: %v", err)
	}
	closer.Hold()
}
